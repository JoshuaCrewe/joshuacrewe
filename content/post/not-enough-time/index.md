---
title: "Not Enough Time"
date: 2022-09-05T16:53:08+01:00
draft: false
---

There is never enough time to learn all the new things you want to and sometimes other things need to be prioritised. At the moment I am trying to prioritise experiencing some non computer related experiences. Mostly I am (slowly) learning Hebrew and trying to get out on my bike more. Also, the sea isn't very far from my house and I really want to be in it all year round. As the weather gets colder I would rather acclimatise than get a shock for the Boxing Day dip. In an attempt to limit distractions here are some things which I want to do when I have the time. (also I can refer back to this post in the future and say I wanted to do them but never got around to it, in case any of them become really cool)

## NixOS

I think that NixOS would solve the following issues, which I know I want to solve on my current system but not enough to justify the work (I also have solutions to some of these things) :

- Rollbacks
- Keeping track of whats installed
- Minimal installs
- Experimentation

## Plasma Bigscreen

This one is mostly for my extended family, but having something which can be a Roku/Fire stick replacement would be neat. I am thinking about a device which can be controlled using a simple hardware UI (like a remote or a games controller) that can handle :

- the big tech streaming platforms (Disney+, Apple TV, Amazon Prime et al.
- Some less big tech stuff (ITV, Channel 4)
- Your own things (Streaming from the NAS, Jellyfin, that sort of thing)

My thoughts are a raspberry pi with [Plasma Bigscreen](https://plasma-bigscreen.org/) on it with a stripped down app selection ~~as well as anbox running~~ (it isn't the right tool for this job). I have never tried any of this so I have no idea if any of it is any good. I am thinking about anbox as a way of simplifying the UI for the mainstream apps as well as boosting (Linux) support. Instead of always opening a browser (and so needed a keyboard and mouse) to type in each streaming service, the android app should act as a nice shortcut to these bits of content. Everyone makes an Android app these days but not always a Linux desktop app. Old people and kids are much more familiar clicking on a logo than they are dealing with URLs (I reckon).

Alternative to Anbox would be to use the wrapper for a webapp which makes website more ... app like. This would definitely work whereas I think Anbox struggles to do stuff like streaming video. I loaded Anbox up on my laptop and it worked very well except that Disney+ didn't play any video.

## Rebuild this website

It has been working fine for years and does a good job. It just isn't all that fun. Looking at peoples websites and how often they change them makes me want to change mine up more. If I had the time I think I would change it up lots more than I do now. In a way, getting the content served in an API from markdown to be consumed by many other things would be a good shout. It would make switching out the website part easier I would think.

I sort of want to make a svelteKit implementation as well as see what 11ty has to offer. Or even Nuxt?! Probably not Nuxt.

## Other things

There is always more to do and never enough time to do it in. We just need to live with this tension.

Peace.
