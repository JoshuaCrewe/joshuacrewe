---
title: "More Browser Extensions on Android"
date: 2023-08-07T10:41:39+01:00
draft: false
---

I have had a tab open in my phone browser for ages which is a [link to an article](https://blog.mozilla.org/addons/2020/09/29/expanded-extension-support-in-firefox-for-android-nightly/) that show you how to install more extensions for Firefox working on Android.

By default there are a handful of extensions you can install which mostly are ad blockers. In the past this hasn't been too much of an issue and every time I have thought about installing something else, I get to this point and give up. You can't install extensions in the same way you can on a desktop.

However I really wanted [consent-o-matic](https://consentomatic.au.dk/) to make browsing sites a bit more enjoyable. It makes shopping a bit easier but really it makes searching lots of sites for a technical solution easier as well. Additionally there is a read it later app I am assessing and they only have an extension to add articles with. 

This was motivation enough and I was able to find the article lined above. It is a fine solution (if you don't mind having a mozilla account), so thank you internet!
