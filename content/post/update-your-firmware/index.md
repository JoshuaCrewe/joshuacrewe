---
title: "Update Your Firmware"
date: 2023-08-07T10:30:20+01:00
draft: false
---

Always do your updates. It is essential for security. If you have good backups then there isn't really anything to lose.

For future me, when you want to update the firmware on your laptop, you should use [fwupd](https://github.com/fwupd/fwupd). 

OK?
