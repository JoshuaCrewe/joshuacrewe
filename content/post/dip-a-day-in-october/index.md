---
title: "Dip a Day in October"
date: 2022-10-03T16:19:13+01:00
draft: false
---

I ~~am taking~~ took up the callenge from Surfers Against Sewage :

> "We challenge you to take a dip a day in cold water every day this October.
> Boost your wellbeing. And fight for the ocean."

_- [SAS Dip A Day](https://www.sas.org.uk/dip-a-day)_

I think this is a great challenge and I am looking forward to the rest of it. Except the cold showers. They aren't fun at all. I will probably take this opportunity to talk about my experiences getting cold which will always mention #dipaday and Surfers Against Sewage. When looking to swim in the sea I have been making use of their map which shows if there is a sewage warning in the area. I am hoping that one day we won't need this warning as our seas will be safe and clean.

As an aside, I have such strong fond memories which involve the SAS brand, my brother used to have a SAS hoodie that I once used to look older in the pub.

## The Breakdown

```
|--------|----------|
| Day    | Location |
|--------|----------|
| 01 Sat | River    |
| 02 Sun | Sea      |
| 03 Mon | Shower   |
| 04 Tue | Sea      |
| 05 Wed | Sea      |
| 06 Thu | Sea      |
| 07 Fri | Sea      |
| 08 Sat | Sea      |
| 09 Sun | Sea      |
| 10 Mon | Sea      |
| 11 Tue | Sea      |
| 12 Wed | Sea      |
| 13 Thu | Sea      |
| 14 Fri | Sea      |
| 15 Sat | Sea      |
| 16 Sun | Sea      |
| 17 Mon | Sea      |
| 18 Tue | Sea      |
| 19 Wed | Sea      |
| 20 Thu | Sea      |
| 21 Fri | Sea      |
| 22 Sat | Sea      |
| 23 Sun | Shower   |
| 24 Mon | Sea      |
| 25 Tue | Sea      |
| 26 Wed | Sea      |
| 27 Thu | Sea      |
| 28 Fri | Sea      |
| 29 Sat | Sea      |
| 30 Sun | Sea      |
| 31 Mon | Sea      |
|--------|----------|

```
