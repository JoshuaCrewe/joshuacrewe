---
title: "Audible Backup"
date: 2022-09-28T15:08:51+01:00
draft: false
---

I was listening to a podcast this morning about Audible and what they do with their books your buy. The person speaking was describing how they used their wifes credit to buy a book and then convert it from the .aax encrypted format to .mp3 so they could listen to it not in the app. As far as they were aware this is legal as audible is actually selling the book rather than a licence to listen to the book for as long as you have an account. This was my understanding as well and there are a bunch of people who backup their Audible purchases in a similar way.

I realised that I was likely to forget this process. I should write it down.

I learned how to do this when helping my brother back up his collection. I knew it was possible but not having an Audible account made this information not very useful to me. I have, in the recent past bought real physical CDs before I considered going to Amazon. Also a plug for your local library, they will let you borrow both physical and digital audiobooks which is an amazing service.

## Premable

Audible encodes their books as `.aax` files which need your audible code to decrypt and play. To convert them to `.mp3` (or similar), this is what you need. There were some scripts that would do it but they didn't work for me. Once I had a file in the `.aax` format I was able to use [a website](https://audible-converter.ml/) to get the Activation Bytes I needed. It was by far more simple than the other methods.

## There were a LOT of books

When I ofered to help with this, I didn't quite realise how many books we were talking about. There were 250 books. To download a book in the Audible UI you need to do it one. at. a. time. OR make use of [OpenAudible](https://openaudible.org/) (not open) to bulk download them. Once all the files are on the machine it is a case of taking the conversion script and pointing it in the right direction.

## The conversion

[AAXtoMP3](https://github.com/KrumpetPirate/AAXtoMP3) is the name of the script I used. There really wasn't much to it after that apart from waiting. It even detected there were lots of `.aax` files and worked through each one.

```
aaxtomp3 --chaptered --authcode xxxxxxx /path/to/folder/with/aax_files/
```

It took about 30 hours over the course of 3 days to convert them all. Really this is a one time process though only repeating for each new book that is bought.

_All content is for educational something something ..._


