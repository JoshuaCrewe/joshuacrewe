---
title: "Tmuxs"
date: 2023-07-16T16:49:37+01:00
summary: "A terminal multiplexer session manager (ish)"
tags: [bash, cli]
---

## The problem

Working in an agency I work on many different projects at once. Having a little look now, I have 169 project installed on my machine. About 70 of them are actively working on regularly with updates to the codebase happening at least once in a three month time span.

I needed a way to be able to get up and running on each of these projects as quickly as possible. Things like :

- Setting the project root
- Doing a `git status` check for ongoing work
- Starting up build tools
- Pointing my editor at the right set of files

All doable but repetitive tasks. One day, instead of doing this 1 minute job, I spent a few hours writing a bash script.

## The Solution

And I am glad I did write that bash script. It is likely that I have earned that time back with a little extra and I still have a bit of my working life ahead of me!

I use [tmux](https://github.com/tmux/tmux) as my terminal multiplexer and I have used it for as long as I can remember. It can spin up windows and panes and splits and tab and all that TUI goodness.

The bash script I wrote accepts one argument which is the type of project that is being set up. These are configuration files (also written in bash) where I can define the type of environment that is needed. Turns out I have only ever made use of one and never needed to have additional profiles although they are there if I need them.

It does some slightly smart things like attach to an existing session if one with the same name exists. This even works inside another session which if you tried without `tmuxs` would squawk at you and tell you how dangerous that is.

`tmuxs web` is enough to create two windows, one split, run a `git status` and a `npx mix --watch` or `gulp`. Then off I go wondering if I will ever need another session profile.

[Source code](https://codeberg.org/JoshuaCrewe/dotfiles/src/branch/main/bin/tmuxs)
[Profiles](https://codeberg.org/JoshuaCrewe/dotfiles/src/branch/main/tmuxs)
