---
title: "Deploy"
date: 2023-07-19T16:49:37+01:00
draft: false
summary: "A wrapper around rsync to move code around"
tags: [bash, cli]
---

## The problem

What is the best way to move source code around? I don't know, but rsync works well. In the past I have been exposed to using the following technologies to deploy code :

- [git](https://git-scm.com/)
- [capistrano](https://github.com/capistrano/capistrano)
- [SFTP](https://en.wikipedia.org/wiki/SSH_File_Transfer_Protocol)
- [git-ftp](https://github.com/git-ftp/git-ftp)
- [Bitbucket Pipelines](https://bitbucket.org/product/features/pipelines)

Capistrano (if I remember correctly) got set up on one project and it was so super complicated that I didn't do it again. I had a lot of success with git-ftp when it wasn't being [broken by a curl update](https://github.com/git-ftp/git-ftp/issues/464#issuecomment-543592404).

Long story short, there is more than one way to skin a cat and rsync has skinned more cats than the others.

## The solution

Rsync as a bunch of flags which all do really helpful things. My current favourite are `-vzcaSlh`. I was able to write a bash script which remembered all these flags and had an interface for managing connections per project. Authentication would be handled using SSH keys with the server details being stored using `git config`.

The end result had a CLI like the following :

```
$ git config deploy.staging.url username@serveraddress:/path/to/webroot/

$ deploy staging

# This is a dry run, it will show you the files it wants to shift but not shift them

$ deploy staging go

# This will run the deployment

$ cat .deploy-ignore

───────┬─────────────────────────
       │ File: .deploy-ignore
───────┼─────────────────────────
   1   │ node_modueles
───────┴─────────────────────────

```

I use this all day everyday and it has yet to let me down.

[Source code currently unavailable because I symlinked it](https://codeberg.org/JoshuaCrewe/dotfiles/src/branch/main/bin/deploy)
