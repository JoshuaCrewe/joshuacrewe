---
title: "Library."
date: 2023-07-20T16:49:37+01:00
draft: false
summary: "An alternative front end to a fantastic service"
tags: [laravel, svelte, api]
---

{{< img website "Library. front page" >}}

## The Problem

The local library is a great resource but the online catalogue was lacking some features which I thought would be useful. It also had some features which I found made the service harder to use. Initially I tried to contact the developers responsible for the site to see if they would accept a patch or if they had an official API available but I was never able to reach them. I even used a real telephone.

The source of most of my frustration was related to touch gestures and navigation. The website was configured to navigate between results when the user swiped left or right on the screen. The gesture detection wasn't that good though and it couldn't tell a swipe up from a swipe left. I would try and scroll down the page but be navigated to the next page of results or the next item in the catalogue. It was frustrating enough to go looking for a solution.

## The Solution

I built an alternative front end to the library catalogue using Laravel and Svelte. With Laravel I was able to create my own API endpoints to browse, borrow, renew, remove and keep track of books. I was able to log in and see what my current reservations are as well as see books which are on hold. There is a section which allows me to see the list of books which I want to read (one day).

The front end is built with Svelte to interact with the API. It features loading animations and all the required UI including search, pagination, login and site navigation.

With this setup I was able to include additional features which are not available in the official catalogue. One feature I included was the ability to stay logged in to my account for longer.

## More?

I did actually [write about this](/post/library) after I had been using it for a good while. Every once in a while I wonder if they will update the underlying website which would break this interface entirely. It hasn't happened yet.

[Source code on Codeberg](https://codeberg.org/JoshuaCrewe/library)
