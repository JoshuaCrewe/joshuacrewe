---
title: "Crewetube."
date: 2023-07-20T17:49:37+01:00
draft: false
summary: "An alternative front end to an alternative front end"
tags: [svelte]
---

{{< img website "Crewetube. front page, an alternative front end to an alternative front end" >}}

## The Problem

I have a handful of young children and they keep getting older, it is most alarming. As they get older they want to do more things. How to protect your children from the internet as well as educate them on how to navigate it safely is a daunting task. Someone introduced them to youTube (If I ever find out who that was ...) which is famously owned by Google. I am not Googles biggest fan and don't use any of their products or services my self.

Because of the privacy concerns, alternative front ends to YouTube has sprung up all over the place. Initially I redirected all traffic intended for youTube to one of these. However there is still a lot of content on the platform which is not suitable for my kids. I needed a way to allow my kids to watch pre approved content without the need to download and store that content myself in something like Jellyfin.

## The Solution

I built and alternative front end to an alternative front end to youTube. I did it in Svelte which made the process nice and quick to get a working website up and running. One of the key features I wanted right from the start was to be able to manage the approved content with a single file on disk. This way I didn't need to boot up a development environment every time I wanted to add a channel or playlist or video.

The UI is intentionally sparse so that it is not overwhelming for little eyes. It also very much wants those little eyes to get out side and play in some grass or something.

[Source code on Codeberg](https://codeberg.org/JoshuaCrewe/CreweTube)
