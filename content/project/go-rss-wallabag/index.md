---
title: "Go RSS to Wallabag"
date: 2023-07-18T16:29:37+01:00
draft: false
summary: "Sending new articles from RSS feeds to a self hosted read it later app"
tags: [go, rss, cli]
---

{{< img ui "Fetching new articles from RSS and sending them to Wallabag" >}}

## The Problem

Read it later applications are great for keeping a track of things which you want to read on the internet. I used to use Pocket all the time, saving tutorials or news about technology which I use day to day.

RSS is a really great system of keeping track of what your favourite websites are pumping out each week. Old as the hills and standardised there are lots of things which will recognise and RSS feed.

There is a [neat project](https://github.com/FraGag/feeds-to-pocket) which took a bunch of RSS feeds and sent the newly released articles off to Pocket.

I stopped using pocket because it wasn't open source and I wasn't a huge fan of the monetization or social integrations. If you are switching from Pocket to something open source then it will probably be [Wallabag](https://www.wallabag.it/en).

So now I am not using Pocket anymore, how do I keep a track of my RSS feeds?

## The Solution

I built a similar tool to the Feeds to Pocket programme but instead of Pocket, it sent them to my Wallabag instance. I wrote it in the Go Programming language because I wanted something that was super quick and compiled down to the single binary without any dependencies. While Go fulfills these requirements, I do sort of regret my decision. I normally have nothing to do with Google for privacy concerns and rationalised this decision on the basis that Go was open source and not a data mining tool. It turns out they will be trying to [mine some data with their compiler](https://github.com/golang/go/discussions/58409). Who would have guessed?!

As it happens, it is super quick and it _does_ compile down to a single binary with no dependencies, so a success on every point.

I made some different architecture decisions from the other tool and I am happy with the way this version works. As it is functional I have never got around to making it feature complete. I am more likely to write another tool in a different language for a different self hosted read it later app than I am to add more features to this one.

[I wrote some more about this on my blog](/post/perfect-rss-reader/)

[The source code](https://github.com/JoshuaCrewe/rss-to-wallabag)
