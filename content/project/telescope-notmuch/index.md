---
title: "Telescope-Notmuch.nvim"
date: 2023-07-17T16:49:37+01:00
summary: "Finding emails with notmuch and Neovim Telscope"
tags: [lua, tui]
---

{{< img ui "a key combination brings up a menu which I can fuzzy search through and fill in email addresses">}}

## The Problem

I use a [text based email client](https://github.com/LukeSmithxyz/mutt-wizard) for everything related to work and email. I would use it for my home email but I only read that on my phone and even then very rarely. At work we are dealing with lots of email all the time and this workflow is pretty much perfect.

There are some things which I missed from a Graphical email client. One of them was being able to autocomplete email addresses based on the emails which have come and gone over the years.

Being text based, most of the actual email writing happens in [the text editor](https://neovim.io/). This includes the To:, CC:, BCC: etc. fields. I needed a way to scan existing emails and get the addresses from them and then present this data in a searchable interface for each of these fields.

## The Solution

Much of this already happens and I was able to bring it all together into one place. Notmuch will already keep a track of my emails and allow me to query the archive for mail based on some inputs. Telescope is a plugin for my text editor that will take data, place it in a list and then allow me to fuzzy search through that list to reduce it to one result.

I was able to write a plugin for Telescope which took a query from notmuch and allow me to fuzzy search through a list of emails and insert the selected email in the focused field.

[I did write about this](/post/telescope-notmuch-nvim/) last summer when I took a run at solving this problem.

[Source Code](https://codeberg.org/JoshuaCrewe/telescope-notmuch.nvim)
